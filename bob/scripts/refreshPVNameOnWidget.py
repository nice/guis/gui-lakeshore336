# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Lakeshore 336
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Standard libraries
# -----------------------------------------------------------------------------
import os, sys, time

from time import sleep
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

logger = ScriptUtil.getLogger()

# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def generatorProcedure():
    pvName = widget.getPropertyValue("pv_name")
    #logger.info(pvName)
    widget.setPropertyValue('pv_name', "")
    widget.setPropertyValue('pv_name', pvName)
    #logger.info("refreshed!")

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
#sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
generatorProcedure()
